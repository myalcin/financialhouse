<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Http\Model\User;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserAuth()
    {
        $user = new User;

        $email = 'merchant@test.com';
        $password = '123*-+';
        $this->assertTrue($user->auth($email, $password));

        $email = 'demo@bumin.com.tr';
        $password = 'cjaiU8CV';
        $this->assertTrue($user->auth($email, $password));
    }
}
