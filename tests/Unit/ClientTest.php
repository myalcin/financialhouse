<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testClientList()
    {
        $response = $this->json('POST', '/api/v3/client/?apiKey=LxfR0SpklNImn924RAjpLf79xBrUxWmxpPoeU0fDjs=');
        $response->assertStatus(401);
    }
}
