<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api_token', 'jwt.verify']], function() {
    Route::post('v3/client/', 'ClientController@listAllClient');
    Route::post('v3/transactions/report/', 'TransactionController@report');
    Route::post('v3/transactions/transaction/', 'TransactionController@transaction');
});

Route::group(['middleware' => ['api_token']], function() {
    Route::post('v3/merchant/user/login/', 'AuthController@login');
});