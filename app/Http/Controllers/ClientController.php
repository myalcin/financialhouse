<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\Client;

class ClientController extends Controller
{
    public function listAllClient(Request $request) 
    {
        $transactionId = $request->get('transactionId');
        if($transactionId === NULL) {
            return response()->json(['error' => 'Invalid parameters!'], 200);
        }

        $client = new Client;
        return response()->json(
            array (
                'customerInfo' => $client->populateClientData(), 
            )
        );
    }

}
