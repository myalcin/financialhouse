<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use JWTFactory;
use JWTAuth;
use App\Http\Model\User;

class AuthController extends Controller
{
    public function login(Request $request) {
        $email = $request->get('email');
        $password = $request->get('password');
        $user = new User;
        if($user->auth($email, $password) === TRUE) {
            $factory = JWTFactory::customClaims([
                'sub'   => env('APP_NAME'),
                'iss'   => config('app.name'),
                'iat'   => Carbon::now()->timestamp,
                'exp'   => Carbon::now()->addMinutes(JWTFactory::getTTL())->timestamp,
                'nbf'   => Carbon::now()->timestamp,
                'jti'   => uniqid(),
            ]);
            $payload = $factory->make();
            $token = JWTAuth::encode($payload);
            return $this->respondWithToken((string)$token);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'status' => 'APPROVED',
        ]);
    }
}
