<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\TransactionReport;
use App\Http\Model\Transaction;


class TransactionController extends Controller
{
    public function report(Request $request) 
    {
        $fromDate = $request->get('fromDate');
        $toDate = $request->get('toDate');
        $merchant = $request->get('merchant');
        $acquirer = $request->get('acquirer');
        if($fromDate === NULL || $toDate === NULL) {
            return response()->json(['error' => 'Invalid parameters!'], 200);
        }
        $transactionReport = new TransactionReport;
        $transactionReport->populateClientData();
        return response()->json(
            array (
                'status' => 'APPROVED',
                'response' => $transactionReport->populateClientData(), 
            )
        );
    }

    public function transaction(Request $request) 
    {
        $transactionId = $request->get('transactionId');
        if($transactionId === NULL) {
            return response()->json(['error' => 'Invalid parameters!'], 200);
        }
        $transaction = new Transaction;
        return response()->json($transaction->populateTransactionData());
    }
}
