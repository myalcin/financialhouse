<?php

namespace App\Http\Middleware;

use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('apiKey') != env('API_KEY')) {
            return response()->json('Unauthorized request!', 401);
        } 
    
        return $next($request);
    }
}
