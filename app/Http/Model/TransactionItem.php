<?php

namespace App\Http\Model;

use App\Http\Model\BaseModel as BaseModel;

class TransactionItem extends BaseModel
{
    protected $count;
    protected $total;
    protected $currency;

    function __construct($_count, $_total, $_currency)  {
        $this->count = $_count;
        $this->total = $_total;
        $this->currency = $_currency;
    }

    public function populateTransactionItemData() {
        return $this->toArray($this);
    }


}
