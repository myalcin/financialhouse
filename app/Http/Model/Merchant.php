<?php

namespace App\Http\Model;

use App\Http\Model\BaseModel as BaseModel;

class Merchant extends BaseModel
{
    protected $referenceNo;
    protected $merchantId;
    protected $status;
    protected $channel;
    protected $customData;
    protected $chainId;
    protected $agentInfoId;
    protected $operation;
    protected $fxTransactionId;
    protected $updated_at;
    protected $created_at;
    protected $id;
    protected $acquirerTransactionId;
    protected $code;
    protected $message;
    protected $transactionId;
    protected $agent;

    public function populateMerchantData () 
    {
        $this->referenceNo = "reference_5617ae66281ee";
        $this->merchantId = 1;
        $this->status = "WAITING";
        $this->channel = "API";
        $this->customData = null;
        $this->chainId = "5617ae666b4cb";
        $this->agentInfoId = 1;
        $this->operation = "DIRECT";
        $this->fxTransactionId = 1;
        $this->updated_at = "2015-10-09 12:09:12";
        $this->created_at = "2015-10-09 12:09:12";
        $this->id = 1;
        $this->acquirerTransactionId = 1;
        $this->code = "00";
        $this->message = "Waiting";
        $this->transactionId = "1-1444392550-1";
        $this->agent = [
            "id" => 1,
            "customerIp" => "192.168.1.2",
            "customerUserAgent" => "Agent",
            "merchantIp "  => "127.0.0.1"
        ];

        return $this->toArray($this);
    }
}
