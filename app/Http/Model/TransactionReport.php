<?php

namespace App\Http\Model;

use App\Http\Model\BaseModel as BaseModel;

class TransactionReport extends BaseModel
{
    protected $count;
    protected $total;
    protected $currency;
    
    public function populateClientData () 
    {
        $data = [];
        $transactionItem = new TransactionItem(283, 28300, 'USD');
        array_push($data, $transactionItem->populateTransactionItemData());
        $transactionItem = new TransactionItem(280, 1636515, 'EUR');
        array_push($data, $transactionItem->populateTransactionItemData());
        return $data;
    }
}
