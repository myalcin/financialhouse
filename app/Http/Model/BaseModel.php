<?php

namespace App\Http\Model;

class BaseModel
{
    protected function toArray($obj)
    {
        $reflection = new \ReflectionObject($obj);
        $properties = $reflection->getProperties();
        $dataKeyValue = [];
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $dataKeyValue[$property->getName()] = $property->getValue($this);
        }
        return $dataKeyValue;
    }
}
