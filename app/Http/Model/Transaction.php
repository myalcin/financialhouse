<?php

namespace App\Http\Model;

use App\Http\Model\BaseModel as BaseModel;
use App\Http\Model\Client as Client;
use App\Http\Model\Merchant as Merchant;

class Transaction extends BaseModel
{
    protected $clientList;
    protected $fx;
    protected $merchant;
    protected $transaction;

    public function populateTransactionData() {

        $this->fx = [
            'merchant' => [
                'originalAmount' => 100,
                'originalCurrency' => 'EUR'
            ]
        ];

        $client = new Client;
        $this->clientList = $client->populateClientData();

        $this->merchant = [
            'name' => 'Dev-Merchant'
        ];

        $merchant = new Merchant;
        $this->transaction = [
            [
                'merchant' => $merchant->populateMerchantData()
                ]
        ];

        return [
            'fx'=> $this->fx,
            'customerInfo' => $this->clientList,
            'merchant' => $this->merchant,
            'transaction' => $this->transaction
        ];
    }


}
