<?php

namespace App\Http\Model;

use App\Http\Model\BaseModel as BaseModel;

class Client extends BaseModel
{
    protected $id;
    protected $created_at;
    protected $updated_at;
    protected $deleted_at;
    protected $number;
    protected $expiryMonth;
    protected $expiryYear; 
    protected $startMonth;
    protected $startYear;
    protected $issueNumber;
    protected $email; 
    protected $birthday; 
    protected $gender; 
    protected $billingTitle; 
    protected $billingFirstName; 
    protected $billingLastName; 
    protected $billingCompany; 
    protected $billingAddress1; 
    protected $billingAddress2; 
    protected $billingCity; 
    protected $billingPostcode; 
    protected $billingState; 
    protected $billingCountry; 
    protected $billingPhone;
    protected $billingFax; 
    protected $shippingTitle; 
    protected $shippingFirstName; 
    protected $shippingLastName; 
    protected $shippingCompany; 
    protected $shippingAddress1; 
    protected $shippingAddress2; 
    protected $shippingCity;
    protected $shippingPostcode; 
    protected $shippingState; 
    protected $shippingCountry;
    protected $shippingPhone; 
    protected $shippingFax; 
    

    public function populateClientData () 
    {
        $this->id = 1;
        $this->created_at = '2015-10-09   12:09:10';
        $this->updated_at = '2015-10-09   12:09:10';
        $this->deleted_at = null;
        $this->number = '401288XXXXXX1881';
        $this->expiryMonth = '6';
        $this->expiryYear = '2017'; 
        $this->startMonth = null;
        $this->startYear = null;
        $this->issueNumber = null;
        $this->email = 'michael@gmail.com'; 
        $this->birthday = '1986-03-20 12:09:10'; 
        $this->gender = null; 
        $this->billingTitle = null; 
        $this->billingFirstName = 'Michael'; 
        $this->billingLastName = 'Kara'; 
        $this->billingCompany = null; 
        $this->billingAddress1 = 'test   address'; 
        $this->billingAddress2 = null; 
        $this->billingCity = 'Antalya'; 
        $this->billingPostcode = '07070'; 
        $this->billingState = null; 
        $this->billingCountry = 'TR'; 
        $this->billingPhone = null;
        $this->billingFax =  null; 
        $this->shippingTitle = null; 
        $this->shippingFirstName = 'Michael'; 
        $this->shippingLastName = 'Kara'; 
        $this->shippingCompany = null; 
        $this->shippingAddress1 = 'test   address'; 
        $this->shippingAddress2 = null; 
        $this->shippingCity = 'Antalya';
        $this->shippingPostcode = '07070'; 
        $this->shippingState = null; 
        $this->shippingCountry = 'TR';
        $this->shippingPhone = null; 
        $this->shippingFax =  null; 

        return $this->toArray($this);
    }
}
